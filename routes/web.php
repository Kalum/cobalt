<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestController@index')->name('home');


Route::prefix('auth')->group(function () {
    Route::get('login', 'AuthController@redirectToProvider')->name('login');
    Route::get('callback', 'AuthController@handleProviderCallback');
    Route::get('logout', 'AuthController@handleLogout')->name('logout');
});

Route::prefix('projects')->group(function () {
    Route::get('/personal', 'GuestController@personal')->name('project_personal');
    Route::get('/games', 'GuestController@games')->name('project_games');
    Route::get('/emoji', 'GuestController@emoji')->name('project_emoji');
    Route::get('/animation', 'GuestController@animation')->name('project_animation');
});

Route::prefix('commissions')->group(function () {
    Route::get('/music', 'GuestController@music')->name('project_music');
    Route::get('/art', 'GuestController@art')->name('project_art');
});

Route::get('/merch', 'GuestController@merch')->name('merch');

Route::get('/updates', 'GuestController@updates')->name('updates');

