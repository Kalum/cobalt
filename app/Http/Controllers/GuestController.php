<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index() { return view('home.index'); }

    public function personal() { return view('projects.personal'); }

    public function games() { return view('projects.games'); }

    public function emoji() { return view('projects.emoji'); }

    public function animation() { return view('projects.animation'); }

    public function music() { return view('commissions.music'); }

    public function art() { return view('commissions.art'); }

    public function merch() { return view('errors.construction'); }

    public function updates() { return view('updates.index'); }
}
